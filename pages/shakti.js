import siteMetadata from '@data/siteMetadata'
import { PageSeo } from '@components/SEO'
import Image from 'next/image.js'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
import { CryptoPay } from '@crypteriat/cryptopay'
import projectsData from '@data/projectsData'

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'shakti',
        'copyButton',
        'donateButton',
        'donateField',
        'payButtons',
        'paypalDonate',
        'footer',
      ])),
    },
  }
}

export default function Shakti() {
  const { t } = useTranslation('shakti')

  const addresses = [
    { ticker: 'lbtc', payserver: projectsData.shakti.payments.lbtc.uri },
    { ticker: 'btc', address: projectsData.shakti.payments.btc },
    { ticker: 'eth', address: projectsData.shakti.payments.eth },
    { ticker: 'bnb', address: projectsData.shakti.payments.bnb },
    { ticker: 'paypal', link: projectsData.shakti.payments.paypal },
  ]

  const paypalOptions = {
    fieldDes:
      'You will be redirected to a PayPal payment website for: Shakti Crypto Collective',
  }

  return (
    <>
      <PageSeo
        title={`Charities - ${projectsData.shakti.href}`}
        description={siteMetadata.description}
        url={`${siteMetadata.siteUrl}/Shakti`}
      />

      <div className="divide-y divide-gray-200 dark:divide-gray-700">
        <div className="pt-6 pb-8 space-y-2 md:space-y-5">
          <p className="text-gray-900 leading-9 dark:text-gray-100 -14">
            {t('description.part1')}
          </p>
        </div>
        <div className="overflow-hidden border-2 border-gray-200 border-opacity-60 dark:border-gray-700 rounded-md">
          <div className="container flex flex-row gap-1">
            <Image
              alt=""
              src="/static/img/Lightning/backside.jpg"
              className="object-cover lg:w-48 md:w-36"
              width={544}
              height={306}
            />
            <Image
              alt=""
              src="/static/img/Lightning/completeNode.jpg"
              className="object-cover lg:w-48 md:w-36"
              width={544}
              height={306}
            />
          </div>
          <div className="p-6">
            <p className="px-3 mb-3 text-gray-500 prose max-w-none dark:text-gray-400">
              {t('description.part2')}
            </p>
          </div>
          <div className="container flex flex-col mx-auto justify-evenly gap-1">
            <Image
              alt="Secured Finance Network"
              src="/static/img/shakti/CongealedFinanceNetwork.svg"
              href="/static/img/shakti/CongealedFinanceNetwork.svg"
              title="Secured Crytocurrency Charity Network"
              description="Secured Crytocurrency Charity Network"
              width={544}
              height={486}
              className="block"
            />
            <p className="px-3 mb-3 text-gray-500 prose max-w-none dark:text-gray-400">
              {t('description.part3')}
            </p>
            <Image
              alt="DeFi service node"
              src="/static/img/shakti/DeFi_Svc_node.png"
              href="/static/img/shakti/DeFi_Svc_node.png"
              title="DeFi Node Image"
              className="block object-scale-down"
              description="DeFi Service Node"
              width={408}
              height={580}
            />
          </div>
          <div className="p-6">
            <p className="mb-3 text-gray-500 prose max-w-none dark:text-gray-400">
              {t('description.part4')}
            </p>
          </div>
          <CryptoPay
            addresses={addresses}
            invoice={true}
            paypalOptions={paypalOptions}
          />
        </div>
      </div>
    </>
  )
}
