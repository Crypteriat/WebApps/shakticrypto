import siteMetadata from '@data/siteMetadata';
import teamMetadata from '@data/teamMetadata';
import SocialIcon from '@components/social-icons';
import { PageSeo } from '@components/SEO';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

export async function getStaticProps({ locale = siteMetadata.locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'team', 'footer'])),
    },
  };
}

export default function Team() {
  const { t } = useTranslation('team');

  return (
    <>
      <PageSeo
        title="Team"
        description="Shakti Crypto Collective Team"
        url={`${siteMetadata.siteUrl}/team`}
      />
      <div className="divide-y">
        <div className="pt-6 pb-8 space-y-2 md:space-y-5">
          <h1 className="text-3xl font-extrabold tracking-tight text-gray-900 leading-9 dark:text-gray-100 sm:text-4xl sm:leading-10 md:text-6xl md:leading-14">
            {t('team')}
          </h1>
        </div>
        <div className="items-start space-y-2">
          <div className="pt-8 pb-8 prose dark:prose-dark max-w-none xl:col-span-2">
            <div className="content-center justify-center text-gray-500 grid lg:grid-cols-3 md:grid-cols-2 gap-4 dark:text-gray-400">
              <span className="self-center max-w-md ">
                <img
                  src={teamMetadata.fatou.image}
                  alt="Fatou"
                  className="object-contain w-full border-double max-h-60"
                />
                <span className="flex mb-3 justify-evenly space-x-4">
                  <span>{teamMetadata.fatou.name}</span>
                  <SocialIcon
                    className="align-middle"
                    kind="linkedin"
                    href={teamMetadata.fatou.linkedin}
                  />
                  <SocialIcon
                    className="self-center"
                    kind="twitter"
                    href={teamMetadata.fatou.twitter}
                  />
                </span>
              </span>

              <span className="self-center max-w-md ">
                <img
                  src={teamMetadata.aisha.image}
                  alt="Aisha"
                  className="object-contain w-full border-double max-h-60"
                />
                <span className="flex mb-3 justify-evenly space-x-4 space-y-4">
                  <span>{teamMetadata.aisha.name}</span>
                  <SocialIcon
                    className="align-middle"
                    kind="linkedin"
                    href={teamMetadata.aisha.linkedin}
                  />
                  <SocialIcon
                    className="self-center"
                    kind="twitter"
                    href={teamMetadata.aisha.twitter}
                  />
                </span>
              </span>

              <span className="self-center max-w-md ">
                <img
                  src={teamMetadata.banel.image}
                  alt="Banel"
                  className="object-contain w-full border-double max-h-60"
                />
                <span className="flex mb-3 justify-evenly space-x-4">
                  <span>{teamMetadata.banel.name}</span>
                </span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
