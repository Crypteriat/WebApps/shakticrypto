import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { getAllFilesFrontMatter } from '@lib/mdx';
import siteMetadata from '@data/siteMetadata';
import ListLayout from '@layouts/ListLayout';
import { PageSeo } from '@components/SEO';

export async function getStaticProps({ locale = siteMetadata.locale }) {
  const meetings = await getAllFilesFrontMatter('meetings');

  return {
    props: {
      meetings,
      ...(await serverSideTranslations(locale, [
        'common',
        'meetings',
        'footer',
      ])),
    },
  };
}

export default function Meetings({ meetings }) {
  const { t } = useTranslation('meetings');

  return (
    <>
      <PageSeo
        title={`Meetings - ${siteMetadata.author}`}
        description={siteMetadata.description}
        url={`${siteMetadata.siteUrl}/meetings`}
      />
      <ListLayout meetings={meetings} title={t('allMeetings')} />
    </>
  );
}
