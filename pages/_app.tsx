import '../css/tailwind.css';
import { useEffect } from 'react';

import { MDXProvider } from '@mdx-js/react';
import { DefaultSeo } from 'next-seo';
import Head from 'next/head.js';
import siteMetadata from '@data/siteMetadata';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { appWithTranslation } from 'next-i18next';
import '@crypteriat/cryptopay/dist/bundle.css'

import { SEO } from '@components/SEO';
import LayoutWrapper from '@components/LayoutWrapper';
import MDXComponents from '@components/MDXComponents';

import useRouterContext from '@lib/RouterContext';
import * as ga from '@lib/gtag';

export async function getStaticProps({ locale = Object.keys(siteMetadata.locales[0])[0] }) { // default
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'footer'])),
    },
  };
}

function _App({ Component, pageProps }) {
  const router = useRouterContext();

  useEffect(() => {
    const handleRouteChange = (url: string) => {
      ga.pageview(url);
    };

    // When the component is mounted, subscribe to router changes
    // and log those page views
    router.events.on('routeChangeComplete', handleRouteChange);

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);

  return (
    <MDXProvider components={MDXComponents}>
      <Head>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
      </Head>
      <DefaultSeo {...SEO} />
      <LayoutWrapper>
        <Component {...pageProps} />
      </LayoutWrapper>
    </MDXProvider>
  );
}

export default appWithTranslation(_App);
