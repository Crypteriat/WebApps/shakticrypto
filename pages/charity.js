import siteMetadata from '@data/siteMetadata';
import projectsData from '@data/projectsData';
import Card from '@components/Card';
import { PageSeo } from '@components/SEO';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

export async function getStaticProps({ locale }) {
  

  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'charity', 'footer'])),
    },
  };
}

export default function Charities() {
  const { t } = useTranslation('charity');

  const projects = Object.keys(projectsData);
  const Cards = projects.map((key) => {
    const item = projectsData[key];
    return (
      <Card
        key={item.title}
        title={t(`${key}.title`)}
        description={t(`${key}.description`)}
        imgSrc={item.imgSrc}
        href={item.href}
      />
    );
  });
  return (
    <>
      <PageSeo
        title={`Charities - ${siteMetadata.author}`}
        description={siteMetadata.description}
        url={`${siteMetadata.siteUrl}/projects`}
      />
      <div className="divide-y divide-gray-200 dark:divide-gray-700">
        <div className="pt-6 pb-8 space-y-2 md:space-y-5">
          <h1 className="text-3xl font-extrabold tracking-tight text-gray-900 md:text-4xl lg:text-6xl leading-9 dark:text-gray-100 sm:leading-10 md:leading-14">
            {t('title')}
          </h1>
          <p className="text-lg text-gray-500 leading-7 dark:text-gray-400">
            {t('description')}
          </p>
        </div>
        <div className="container py-12">
          <div className="flex flex-wrap m-4">{Cards}</div>
        </div>
      </div>
    </>
  );
}
