import siteMetadata from '@data/siteMetadata'
import { PageSeo } from '@components/SEO'
import Image from 'next/image.js'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
import { CryptoPay } from '@crypteriat/cryptopay'
import Link from '@components/Link'
import projectsData from '@data/projectsData'

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'seno_palel',
        'copyButton',
        'donateButton',
        'donateField',
        'payButtons',
        'paypalDonate',
        'footer',
      ])),
    },
  }
}

export default function SenoPalel() {
  const { t } = useTranslation('seno_palel')

  const addresses = [
    { ticker: 'lbtc', payserver: projectsData.seno_palel.payments.lbtc.uri },
    { ticker: 'btc', address: projectsData.seno_palel.payments.btc },
    { ticker: 'eth', address: projectsData.seno_palel.payments.eth },
    { ticker: 'bnb', address: projectsData.seno_palel.payments.bnb },
    { ticker: 'paypal', link: projectsData.seno_palel.payments.paypalLink },
  ]

  const paypalOptions = {
    fieldDes:
      'You will be redirected to a PayPal payment website for: Seno Palel Association',
  }

  return (
    <>
      <PageSeo
        title={`Charities - ${projectsData.seno_palel.href}`}
        description={projectsData.seno_palel.description}
        url={`${siteMetadata.siteUrl}/SenoPalel`}
      />

      <div className="divide-y divide-gray-200 dark:divide-gray-700">
        <div className="pt-6 pb-8 space-y-2 md:space-y-5">
          <p className="text-gray-900 leading-9 dark:text-gray-100">
            {t('description.part1')}
          </p>
        </div>
        <div className="overflow-hidden border-2 border-gray-200 border-opacity-60 dark:border-gray-700 rounded-md">
          <div className="container flex flex-row gap-1">
            <Image
              alt=""
              src="/static/img/Seno_Palel/Supplies.png"
              className="object-cover lg:w-48 md:w-36"
              width={544}
              height={306}
            />
            <Image
              alt=""
              src="/static/img/Seno_Palel/Children.png"
              className="object-cover lg:w-48 md:w-36"
              width={544}
              height={306}
            />
          </div>
          <div className="p-6">
            <p className="mb-3 text-gray-500 prose max-w-none dark:text-gray-400">
              {t('description.part1')}{' '}
              <Link href="/team">Mame Fatou Kouate, </Link>{' '}
              {t('description.part1')}
            </p>
          </div>
          <CryptoPay
            addresses={addresses}
            invoice={true}
            paypalOptions={paypalOptions}
          />
        </div>
      </div>
    </>
  )
}
