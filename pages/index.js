import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { getAllFilesFrontMatter } from '@lib/mdx';
import siteMetadata from '@data/siteMetadata';
import Tag from '@components/Tag';
import Link from '@components/Link';
import { PageSeo } from '@components/SEO';

const MAX_DISPLAY = 5;
const postDateTemplate = { year: 'numeric', month: 'long', day: 'numeric' };

export async function getStaticProps({ locale = siteMetadata.locale }) {
  const posts = await getAllFilesFrontMatter('meetings');

  return {
    props: {
      posts,
      ...(await serverSideTranslations(locale, ['common', 'index', 'footer'])),
    },
  };
}

export default function Home({ posts }) {
  const { t } = useTranslation('index');

  return (
    <>
      <PageSeo
        title={siteMetadata.title}
        description={siteMetadata.description}
        url={siteMetadata.siteUrl}
      />
      <div className="divide-y divide-gray-200 dark:divide-gray-700">
        <h1 className="text-2xl sm:text-md leading-2 dark:text-gray-400">
          {t('header')}
        </h1>
        <div className="pt-6 pb-8 space-y-2 md:space-y-5">
          <span className="tracking-tight text-gray-900 dark:text-gray-100 sm:leading-10">
            <Link
              href="https://www.clubhouse.com/event/MwJndVGY"
              className="text-black 900 dark:text-gray-100"
            >
              <p className="max-w-2xl pb-3 text-2xl text-center">
                {t('meeting.title')}
              </p>
            </Link>
            <p className="text-lg">
              <Link href="https://www.joinclubhouse.com/club/wwwshakticryptoorg">
                {t('clubhouse')}
                https://www.joinclubhouse.com/club/wwwshakticryptoorg
              </Link>
            </p>
          </span>
          <span className="text-xl text-gray-700 md:text-md sm:text-sm leading-7 dark:text-gray-400">
            {t('meeting.description')}
            <li>{t('meeting.note1')}</li>
            <li>{t('meeting.note2')}</li>
          </span>
        </div>
        <ul className="divide-y divide-gray-200 dark:divide-gray-700">
          {posts.length === 0 && 'No posts found.'}
          {posts.slice(0, MAX_DISPLAY).map((frontMatter) => {
            const { slug, date, title, summary, tags } = frontMatter;
            return (
              <li key={slug} className="py-12">
                <article>
                  <div className="space-y-2 xl:grid xl:grid-cols-4 xl:space-y-0 xl:items-baseline">
                    <dl>
                      <dt className="sr-only">Published on</dt>
                      <dd className="text-base font-medium text-gray-500 leading-6 dark:text-gray-400">
                        <time dateTime={date}>
                          {new Date(date).toLocaleDateString(
                            siteMetadata.locale,
                            postDateTemplate
                          )}
                        </time>
                      </dd>
                    </dl>
                    <div className="space-y-5 xl:col-span-3">
                      <div className="space-y-6">
                        <div>
                          <h2 className="text-2xl font-bold tracking-tight leading-8">
                            <Link
                              href={`/meetings/${slug}`}
                              className="text-gray-900 dark:text-gray-100"
                            >
                              {title}
                            </Link>
                          </h2>
                          <div className="flex flex-wrap">
                            {tags.map((tag) => (
                              <Tag key={tag} text={tag} />
                            ))}
                          </div>
                        </div>
                        <div className="text-gray-500 prose max-w-none dark:text-gray-400">
                          {summary}
                        </div>
                      </div>
                      <div className="text-base font-medium leading-6">
                        <Link
                          href={`/meetings/${slug}`}
                          className="text-blue-500 hover:text-blue-600 dark:hover:text-blue-400"
                          aria-label={`Read "${title}"`}
                        >
                          {t('Read more ')}&rarr;
                        </Link>
                      </div>
                    </div>
                  </div>
                </article>
              </li>
            );
          })}
        </ul>
      </div>
      {posts.length > MAX_DISPLAY && (
        <div className="flex justify-end text-base font-medium leading-6">
          <Link
            href="/meetings"
            className="text-blue-500 hover:text-blue-600 dark:hover:text-blue-400"
            aria-label="all posts"
          >
            {t('allPosts')} &rarr;
          </Link>
        </div>
      )}
    </>
  );
}
