import siteMetadata from '@data/siteMetadata.js';
import { PageSeo } from '@components/SEO';
import Image from 'next/image.js';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { CryptoPay } from '@crypteriat/cryptopay';
import Link from '@components/Link';
import projectsData from '@data/projectsData';

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'braille',
        'copyButton',
        'donateButton',
        'donateField',
        'payButtons',
        'paypalDonate',
        'footer',
      ])),
    },
  };
}

export default function Braille() {
  const { t } = useTranslation('braille');

  const addresses = [
    {
      ticker: 'lbtc',
      payserver: projectsData.braille.payments.lbtc.uri,
    },
    {
      ticker: 'btc',
      address: projectsData.braille.payments.btc,
    },
    {
      ticker: 'eth',
      address: projectsData.braille.payments.eth,
    },
    {
      ticker: 'bnb',
      address: projectsData.braille.payments.bnb,
    },
    {
      ticker: 'paypal',
      link: projectsData.braille.payments.paypal,
    },
  ];

  const paypalOptions = {
    fieldDes:
      'You will be redirected to a PayPal payment website for: FMD Pikine East 92 Association',
  };

  return (
    <>
      <PageSeo
        title={`Charities - ${projectsData.braille.href}`}
        description={projectsData.braille.description}
        url={`${siteMetadata.siteUrl}/Braille`}
      />
      <div className="items-center divide-y divide-gray-200 dark:divide-gray-700">
        <div className="pt-6 pb-8 space-y-2 md:space-y-5">
          <div>
            <Link key="Aisha" href="/team">
              <Image
                alt=""
                src="/static/img/team/Aissata_model.png"
                className="object-cover mx-auto lg:w-48 md:w-36"
                width={170}
                height={206}
              />
            </Link>
          </div>
          <p className="text-gray-900 leading-9 dark:text-gray-100 -14">
            {t('description.introduction')}
          </p>
          <p className="text-gray-900 leading-9 dark:text-gray-100 -14">
            {t('description.part1')}
          </p>
        </div>
        <div className="overflow-hidden border-2 border-gray-200 border-opacity-60 dark:border-gray-700 rounded-md">
          <div className="container flex flex-row">
            <Image
              alt=""
              src="/static/img/Braille_embosser.jpg"
              className="object-cover lg:w-48 md:w-36"
              width={544}
              height={306}
            />
            <Image
              alt=""
              src="/static/img/Braille_hands.jpg"
              className="object-cover lg:w-48 md:w-36"
              width={544}
              height={306}
            />
          </div>
        </div>
        <div className="p-6">
          <p className="mb-3 text-gray-500 prose max-w-none dark:text-gray-400">
            {t('description.part2')}
          </p>
          <p className="mb-3 text-gray-500 prose max-w-none dark:text-gray-400">
            {t('description.part3')}
          </p>
          <p className="mb-3 text-gray-500 prose max-w-none dark:text-gray-400">
            {t('description.close')}
          </p>
          <p className="mb-3 text-gray-500 prose max-w-none dark:text-gray-400">
            {t('description.thank')}
          </p>
          <CryptoPay
            addresses={addresses}
            invoice={true}
            paypalOptions={paypalOptions}
          />
        </div>
      </div>
    </>
  );
}
