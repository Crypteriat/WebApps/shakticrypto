const { i18n } = require('./next-i18next.config')
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})
const withTM = require('next-transpile-modules')(['@crypteriat/cryptopay'])

module.exports = withTM(
  withBundleAnalyzer({
    i18n,
    reactStrictMode: true,
    trailingSlash: true,
    pageExtensions: ['js', 'jsx', 'md', 'mdx', 'ts', 'tsx'],
    future: {},
    webpack: (config, { dev, isServer }) => {
      config.module.rules.push(
        {
          test: /\.(png|jpe?g|gif|mp4)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                publicPath: '/_next',
                name: 'static/media/[name].[hash].[ext]',
              },
            },
          ],
        },
        {
          test: /\.svg$/,
          use: ['@svgr/webpack'],
        },
        {
          test: /\.(ts|tsx)$/,
          include: [/node_modules/],
          use: [
            {
              loader: 'ts-loader',

              // options: {
              //   transpileOnly: true,
              //   experimentalWatchApi: true,
              //   onlyCompileBundledFiles: true,
              // },
            },
          ],
        },
      )
      return config
    },
    /* TODO: This was put in originally to do a rewrite to / with index; NextJS seems to longer support /index or doesn't with TypeScript */
    async rewrites() {
      return {
        beforeFiles: [
          // These rewrites are checked after headers/redirects
          // and before pages/public files which allows overriding
          // page files
          {
            source: '/Braile',
            destination: '/braille',
          },
          {
            source: '/Team',
            destination: '/team',
          },
        ],
      }
    },
    async redirects() {
      return [
        {
          source: '/post',
          destination: '/meetings',
          permanent: true,
        },
      ]
    },
  }),
)
