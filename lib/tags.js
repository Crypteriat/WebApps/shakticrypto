import path from 'path';
import fs from 'fs';
import matter from 'gray-matter';
import { kebabCase } from './utils.js';

const root = process.cwd();

export async function getAllTags(type) {
  const files = fs.readdirSync(path.join(root, 'data', type));

  const tagCount = {};
  // Iterate through each post, putting all found tags into `tags`
  for (const file of files) {
    const source = fs.readFileSync(path.join(root, 'data', type, file), 'utf8');
    const { data } = matter(source);
    if (data.tags && data.draft !== true) {
      for (const tag of data.tags) {
        const formattedTag = kebabCase(tag);
        if (formattedTag in tagCount) {
          tagCount[formattedTag] += 1;
        } else {
          tagCount[formattedTag] = 1;
        }
      }
    }
  }

  return tagCount;
}
