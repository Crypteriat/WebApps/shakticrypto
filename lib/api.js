const port = process.env.port ? process.env.port : '8000';
const ServiceAPI = process.env.ServiceAPI;

function api() {
  const server
    = ServiceAPI === 'development'
      ? `localhost:${port}`
      : `${ServiceAPI}:${port}`;
  console.log(server);
}

export default api;
