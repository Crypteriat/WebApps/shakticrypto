// Log the pageview with their URL
export const pageview = url => {
  if (window) {
    window.gtag('config', process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS, {
      page_path: url, // eslint-disable-line camelcase
    });
  }
};

// Log specific events happening.
export const event = ({action, params}) => {
  window.gtag('event', action, params);
};
