import path from 'path';
import fs from 'fs';
import matter from 'gray-matter';
import visit from 'unist-util-visit';
import readingTime from 'reading-time';
import renderToString from 'next-mdx-remote/render-to-string.js';

import MDXComponents from '@components/MDXComponents';
import imgToJsx from '@lib/img-to-jsx';

const root = process.cwd();

const tokenClassNames = {
  tag: 'text-code-red',
  'attr-name': 'text-code-yellow',
  'attr-value': 'text-code-green',
  deleted: 'text-code-red',
  inserted: 'text-code-green',
  punctuation: 'text-code-white',
  keyword: 'text-code-purple',
  string: 'text-code-green',
  function: 'text-code-blue',
  boolean: 'text-code-red',
  comment: 'text-gray-400 italic',
};

export async function getFiles(type) {
  return fs.readdirSync(path.join(root, 'data', type));
}

/**
 * Remove yyyy-mm-dd and extension in file path to generate slug
 */
export function formatSlug(slug) {
  const regex = /(\d{4})-(\d{2})-(\d{2})-/g;
  return slug.replace(regex, '').replace(/\.(mdx|md)/, '');
}

export function dateSortDesc(a, b) {
  if (a > b) {
    return -1;
  }

  if (a < b) {
    return 1;
  }

  return 0;
}

export async function getFileBySlug(type, slug) {
  const file = fs
    .readdirSync(path.join(root, 'data', type))
    .find((f) => f.includes(slug));
  const source = fs.readFileSync(path.join(root, 'data', type, file), 'utf8');

  const { data, content } = matter(source);
  const mdxSource = await renderToString(content, {
    components: MDXComponents,
    mdxOptions: {
      remarkPlugins: [
        require('remark-slug'),
        require('remark-autolink-headings'),
        require('remark-code-titles'),
        require('remark-math'),
        imgToJsx,
      ],
      inlineNotes: true,
      rehypePlugins: [
        require('rehype-katex'),
        require('@mapbox/rehype-prism'),
        () => (tree) => {
          visit(tree, 'element', (node) => {
            const [token, type] = node.properties.className || [];
            if (token === 'token') {
              node.properties.className = [tokenClassNames[type]];
            }
          });
        },
      ],
    },
  });

  return {
    mdxSource,
    frontMatter: {
      wordCount: content.split(/\s+/gu).length,
      readingTime: readingTime(content),
      slug: slug || null,
      fileName: file,
      ...data,
    },
  };
}

export async function getAllFilesFrontMatter(type) {
  const files = fs.readdirSync(path.join(root, 'data', type));

  const allFrontMatter = [];

  for (const file of files) {
    const source = fs.readFileSync(path.join(root, 'data', type, file), 'utf8');
    const { data } = matter(source);
    if (data.draft !== true) {
      allFrontMatter.push({ ...data, slug: formatSlug(file) });
    }
  }

  return allFrontMatter.sort((a, b) => dateSortDesc(a.date, b.date));
}
