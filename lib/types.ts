export interface PostUpdateTemplate {
  year?: any;
  month?: any;
  weekday?: any;
  day?: any;
}

export interface payButton {
  ticker: String;
  addressOrLink: String | Object;
  setAmount: Function;
  setEmail: Function;
  setQRMessage: Function;
  color?: String;
  description?: String | Object;
}
