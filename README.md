# shakticrypto

Simple website for Shakti

- Meetings
- Slides for Meetings
- Crypto Tracking List
- Whitepaper reviews
- Disclaimer
- Clubhouse, YouTube & Podcast links

```
npm i # install dependencies
npm run dev # local development
npm run build # sets up build using NextJS
```
