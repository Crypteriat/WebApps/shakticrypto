module.exports = {
  title: 'Shakti Crypto',
  author: 'Crypteriat, Inc.',
  headerTitle: 'Shakti Crypto',
  description:
    'Blockchain Education and Crypto Investments for the Developing World',
  siteUrl: 'https://www.shakticrypto.org',
  siteRepo: 'https://gitlab.com/crypteriat/webapp/shakticrypto',
  image: '/static/img/avatar.png',
  logo: '/static/img/shakti.jpg',
  btc: '/static/img/BTC.png',
  btcAddr: '3QfVhBcyCsi7gBvv9zVxvh8HnGfo1dEfUE',
  btcLink:
    'https://blockchain.com/btc/address/3QfVhBcyCsi7gBvv9zVxvh8HnGfo1dEfUE',
  eth: '/static/img/ETH.png',
  ethAddr: '0x6520a28adcd29C8E52007957160E5d2aEbF32a12',
  ethLink:
    'https://etherscan.io/address/0x6520a28adcd29C8E52007957160E5d2aEbF32a12',
  paypalBraille: '/static/img/braille/PayPalBrailleQR.png',
  paypalLink: 'https://www.paypal.com/donate?hosted_button_id=FXWD6FYLKJFJJ',
  paypalDonate: '/static/img/paypal_donate_lg.png',
  paypal: '/static/img/braille/PayPalBrailleQR.png',
  braille: {
    btc: '/static/img/braille/BTC.png',
    btcLink:
      'https://blockchain.com/btc/address/16Z2M7cW5xtSBFc9FnVWD7SL65MQ2JGW1P',
    eth: '/static/img/braille/ETH.png',
    ethLink:
      'https://etherscan.io/address/0xFba0f8a9a25F4A9C90346C4f7A582f9EDE6D9442',
    bnb: '/static/img/braille/BNB.png',
    bnbLink:
      'https://etherscan.io/address/0xFba0f8a9a25F4A9C90346C4f7A582f9EDE6D9442',
  },
  socialBanner: '/static/img/twitter-card.png',
  email: '5647553-jlt-crypteriat@users.noreply.gitlab.com',
  gitlab: 'https://gitlab.com/Crypteriat/WebApps/shakticrypto',
  twitter: 'https://twitter.com/realCrypteriat',
  linkedin: 'https://www.linkedin.com/in/crypteriat',
  locales: [{ en: 'English' }, { fr: 'français' }],
  language: 'English',
};
