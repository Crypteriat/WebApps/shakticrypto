---
title: Our First Shakti Crypto Clubhouse
date: '2021-05-15'
tags: ['clubhouse']
draft: false
summary: We're just getting started...
images: ['/static/img/logo..png']
---

This our first Clubhouse meeting and we want to familiarize ourselves with the tools and website.

## Rules

We will establish rules for our upcoming club which will likely include discussion around "crypto capitalism" and NOT "woke capitalism." Otherwise, regular Clubhouse rules will apply to discussions.

## Our Philosophy

We intend to review, and support cryptocurrency and blockchain projects which fully support civil rights and decentralization. Initiatives emphasizing patents, a limited number of nodes (.e.g "PBFT"),
or pre-mined tokens will likely not be included.

## Summary

This was our first meeting. We were primarily focused on reviewing the website and getting the initiative setup.
Please stay informed and follow us on [Clubhouse](https://www.joinclubhouse.com/event/Md3lw4y5).

## References

https://www.joinclubhouse.com/event/Md3lw4y5
