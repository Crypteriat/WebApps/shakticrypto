---
title: Lightning financial infrastructure & Internet service in Africa as well as the developing world
date: '2021-06-20'
tags:
  [
    'clubhouse',
    'Bitcoin',
    'charity',
    'blind',
    'regulation',
    'Internet',
    'developing world',
    'Africa',
    'West Africa',
  ]
draft: false
summary: Discussion of Lightning network and Internet access in Africa and beyond
images: ['/meeting-data/images/MultiJurisdicitonCompact.png']
---

Cryto Currency and Bitcoin in West Africa

## Agenda

- Lightning Network in W. Africa
- Internet service in the developing world along with Satellite services such as Starlink
- Braille Embossing Machine for Senegalese Children
- Open Questions

## Links

[Satellite Internet Beam Coverage](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8933127)

## Summary

We discussed the opportunity for Lightning Bitcoin in Africa and how satellite Internet might best serve the developing world.

## References

https://www.joinclubhouse.com/club/wwwshakticryptoorg
