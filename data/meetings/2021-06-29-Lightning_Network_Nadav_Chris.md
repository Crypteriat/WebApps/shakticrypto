---
title: Lightning Network discussion with Chris Stewart & Nadav Kohen
date: '2021-06-29'
tags:
  [
    'KYC',
    'AML',
    'cryptocurrency',
    'lightning network',
    'exchange',
    'charity',
    'Senegal',
    'Finance Ministry',
    'Machine Learning',
    'West Africa',
  ]
draft: false
summary: Discussion of Requirements for Anti-Money Laundering and Know Your Customer Regulations for Establishing the Shakti Crypto Charity Network
---

We discussed some of the newer technologies and the evolution of Lightning

## Agenda

- Requirements for Crypto Capitalism in Senegal
- Financial Crimes Enforcement
- Different Blockchains to Supported 
- Open Source AML Models
- Opportunities for Insuring Charity Network

## Links
[AML Sim](https://github.com/IBM/AMLSim)

## Summary

Our first Shakti Crypto Meeting of August planned the introduction of the charity network into West Africa and reviewed how setup anti-money laundering,
know your customer as well as providing employment opportunities for Senegalese citizens.

## Discussion



## References

https://www.joinclubhouse.com/club/wwwshakticryptoorg
