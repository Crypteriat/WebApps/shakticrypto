---
title: Babita Wade, Braille Machine, Best Wallets
date: '2021-05-25'
tags:
  [
    'clubhouse',
    'Babita Wade',
    'Bitcoin',
    'charity',
    'blind',
    'regulation',
    'government',
    'West Africa',
  ]
draft: false
summary: A Conversation with Babita Wade & Kick-Off of our Charitable Efforts
images: ['/meeting-data/images/MultiJurisdicitonCompact.png']
---

Cryto Currency and Bitcoin in West Africa

## Agenda

- Buying Bitcoin in Africa with Babita Wade
- Shakti Crypto Charities
- Braille Machine for Blind Senegalese Children
- Best Begginner Wallets
- Open Questions

## Links

[Exodus Wallet](https://www.exodus.com/)

## Summary

We discussed the opportunity for Bitcoin in Africa and how households can most easily purchase crypto currency in Senegal. In addition, we reviewed our charitable initiatives as well as the Exodus wallet.

## References

https://www.joinclubhouse.com/club/wwwshakticryptoorg
