---
title: Aly Sosagui, BTC Pizza Day, Braille Machine
date: '2021-05-22'
tags: ['clubhouse', 'tax compliance', 'Aly Sosagui', 'legal', 'government', 'Africa']
draft: false
summary: A Conversation with Aly Sosagui, & BTC Pizza Day
images: ['/meeting-data/images/MultiJurisdicitonCompact.png']
---

This is our kick off meeting.

## Agenda

- Conversation with Aly
- Celebrating Bitcoin Pizza Day Giveaway
- Senegalese Charity
- Open Questions

## Links

[Musk & Doge Coin Developers](https://hypebeast.com/2021/5/elon-musk-working-with-dogecoin-developers-since-2019-news)

## Summary

We gave away pizzas and started our charitable initiative with the braille machine. In addition, we had a discussion around legal, tax and regulatory issues related to buying and using crypto currencies in West Africa.

## References

https://www.joinclubhouse.com/club/wwwshakticryptoorg
