---
title: Crypto Charity Discussion
date: '2021-06-12'
tags:
  [
    'cryptocurrency',
    'lightning network',
    'charity',
    'regulation',
    'developing world charities',
    'Africa',
    'non-profit regulatory compliance',
    '3rd World compliance',
    'IRS compliance',
  ]
draft: false
summary: Initial discussion regarding establishing a compliance framework for charities
---

Setting up a Crypto Charity Network for funding worthy initiatives

## Agenda

- Lightning Network as a basis for distributing money to charity
- Internet service in rural areas of Africa
- Ensuring KYC/AML compliance in charitable endeavors
- Open Questions

## Links

## Summary

We discussed the opportunity for Lightning Bitcoin and other cryptocurrencies as a basis for fund-raising and local currency distirbution to charities.

## References

https://www.joinclubhouse.com/club/wwwshakticryptoorg
