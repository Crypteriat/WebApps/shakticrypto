---
title: Our Kick-Off Meeting
date: '2021-05-17'
tags: ['clubhouse', 'doge coin', 'network bound proof of work', 'tax compliance', 'wallets']
draft: false
summary: Kick-Off Meeting and Housekeeping
images: ['/meeting-data/images/MultiJurisdicitonCompact.png']
---

This is our kick off meeting.

## Agenda

- Establishing Shakti Crypto Club
- Using Slido with Clubhouse Meetings
- Investing and Tax Compliance
- Doge Coin, Elon Musk & Network Bound Proof of Work
- Cold vs. Hardware Wallets
- Open Questions

## Links

[Musk & Doge Coin Developers](https://hypebeast.com/2021/5/elon-musk-working-with-dogecoin-developers-since-2019-news)

## Summary

We discussed the above topics and will establish a Shakti Crypto Club with Aly Sosuqa's help. In addition,
we'll setup a follow-on meeting to agree to the group's investment philosophy and tax compliance approach.

## References

https://www.joinclubhouse.com/event/Md3lw4y5
