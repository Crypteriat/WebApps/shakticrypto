---
title: Crypto Charity Network Regulatory Compliance
date: '2021-06-22'
tags:
  [
    'clubhouse',
    'cryptocurrency',
    'lightning network',
    'charity',
    'blind',
    'regulation',
    'Internet',
    'developing world',
    'Africa',
    'West Africa',
    'non-profit regulatory compliance',
  ]
draft: false
summary: Determining how to make regulatorily compliant crypto charities
---

Setting up a Crypto Charity Network for funding worthy initiatives

## Agenda

- Lightning Network as a basis for distributing money to charity
- Internet service in rural areas of Africa
- Ensuring KYC/AML compliance in charitable endeavors
- Open Questions

## Links

## Summary

We discussed the opportunity for Lightning Bitcoin and other cryptocurrencies as a basis for fund-raising and local currency distirbution to charities.

## References

https://www.joinclubhouse.com/club/wwwshakticryptoorg
