module.exports = {
  banel: {
    image: '/static/img/team/BanelSall.png',
    name: 'Banel Sall',
  },
  fatou: {
    image: '/static/img/team/Mame_Fatou_Kouate.png',
    linkedin: 'https://www.linkedin.com/in/mame-fatou-kouate-5aa30065/',
    twitter: 'https://twitter.com/kouame_fatou',
    name: 'Mame Fatou Kouate',
  },
  aisha: {
    image: '/static/img/team/Aissata_Sall.png',
    linkedin: 'https://www.linkedin.com/in/aishasall/',
    twitter: 'https://twitter.com/pullargirl',
    name: 'Aissata Sall',
  },
};
