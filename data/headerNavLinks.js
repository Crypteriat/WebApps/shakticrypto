const headerNavLinks = [
  { href: '/meetings', title: 'Meetings' },
  { href: '/team', title: 'Team' },
  { href: '/tags', title: 'Tags' },
  { href: '/charity', title: 'Charity' },
];

export default headerNavLinks;
