const projectsData = {
  shakti: {
    title: 'Crypto Charity Network for the Developing World',
    description:
      'Building a cryptocurrency charity network to support the new payment ecosystem in the developing and developed world.',
    href: '/shakti',
    imgSrc: '/static/img/Lightning/touchscreen.jpg',
    payments: {
      btc: 'bc1q2ea2neesm76es0f4n0pn5wydn4atxfs69saksf', // Banel's wallet
      lbtc: { apiKey: process.env.lbtcAPIkey, uri: process.env.ltbtcURI },
      eth: '0xFba0f8a9a25F4A9C90346C4f7A582f9EDE6D9442',
      bnb: 'bnb1lxdsmdkc852xtazvnfz8dsrness6larumz8wmw',
      paypal: 'https://www.paypal.com/donate?hosted_button_id=FXWD6FYLKJFJJ',
    },
  },
  braille: {
    title: 'Braille Embossing Machine for Senegalese Schoolchidren',
    description:
      'The FMD Pikine East 92 Association, in Senegal, is raising money to purchase braille machines for Senegalese school children.',
    imgSrc: '/static/img/Braille_hands.jpg',
    href: '/braille',
    payments: {
      btc: '16Z2M7cW5xtSBFc9FnVWD7SL65MQ2JGW1P', // Aisha's wallet
      lbtc: { apiKey: process.env.lbtcAPIkey, uri: process.env.ltbtcURI },
      eth: '0xFba0f8a9a25F4A9C90346C4f7A582f9EDE6D9442',
      bnb: 'bnb1lxdsmdkc852xtazvnfz8dsrness6larumz8wmw',
      paypal: 'https://www.paypal.com/donate?business=XP6J5JVTUBJAQ&no_recurring=0&item_name=FMD+Pikine+East+92+Association&currency_code=USD',
    },
  },
  seno_palel: {
    title:
      'Healthcare, Food and Water Security for Seno Palel, a Sub-Saharan West African Village',
    description:
      'Seno Palel Association needs help with drilling a well, food, and building a hospital. All donations go directly to village elders to support infrastructure for the community.',
    href: '/seno_palel',
    imgSrc: '/static/img/Seno_Palel/Supplies.png',
    payments: {
      btc: '16Z2M7cW5xtSBFc9FnVWD7SL65MQ2JGW1P',
      lbtc: { apiKey: process.env.lbtcAPIkey, uri: process.env.ltbtcURI },
      eth: '0xFba0f8a9a25F4A9C90346C4f7A582f9EDE6D9442',
      bnb: 'bnb1lxdsmdkc852xtazvnfz8dsrness6larumz8wmw',
      paypal: 'https://www.paypal.com/donate?hosted_button_id=FXWD6FYLKJFJJ',
    },
  },
  internet: {
    title: 'Freemium Open Spectrum Internet',
    description:
      'Open Source project focused on providing Freemium Internet for the developing and developed world.',
    imgSrc: '/static/img/open-spectrum.png',
    href: 'https://www.nexchain.org',
  },
};

export default projectsData;
