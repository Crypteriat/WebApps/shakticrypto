import Link from 'next/link.js';
import {kebabCase} from '@lib/utils';
import PropTypes from 'prop-types';

const Tag = ({text}) => (
  <Link href={`/tags/${kebabCase(text)}`}>
    <a className="mr-3 text-sm font-medium text-blue-500 uppercase hover:text-blue-600 dark:hover:text-blue-400">
      {text.split(' ').join('-')}
    </a>
  </Link>
);

Tag.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Tag;
