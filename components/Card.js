import Image from 'next/image.js';
import Link from '@components/Link';
import PropTypes from 'prop-types';
import { useTranslation } from 'next-i18next';

export default function Card({ title, description, imgSrc, width = 544, height  = 306, href }) {
  const { t } = useTranslation('charity');

  return (
    <div className="p-4 md:w-1/2 md" style={{ maxWidth: '544px' }}>
      <div className="h-full overflow-hidden border-2 border-gray-200 border-opacity-60 dark:border-gray-700 rounded-md">
        {href ? (
          <Link href={href} aria-label={`Link to ${title}`}>
            <Image
              alt={title}
              src={imgSrc}
              className="object-cover object-center lg:h-48 md:h-36"
              width={width}
              height={height}
            />
          </Link>
        ) : (
          <Image
            alt={title}
            src={imgSrc}
            className="object-cover object-center lg:h-48 md:h-36"
            width={width}
            height={height}
          />
        )}
        <div className="p-6">
          <h2 className="mb-3 text-2xl font-bold tracking-tight leading-8">
            {href ? (
              <Link href={href} aria-label={`Link to ${title}`}>
                {title}
              </Link>
            ) : (
              title
            )}
          </h2>
          <p className="mb-3 text-gray-500 prose max-w-none dark:text-gray-400">
            {description}
          </p>
          {href && (
            <Link
              href={href}
              className="text-base font-medium text-blue-500 leading-6 hover:text-blue-600 dark:hover:text-blue-400"
              aria-label={`Link to ${title}`}
            >
              {t('card.lm')} &rarr;
            </Link>
          )}
        </div>
      </div>
    </div>
  );
}

Card.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  imgSrc: PropTypes.string.isRequired,
  href: PropTypes.string,
};
