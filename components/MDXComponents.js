import Image from 'next/image.js';
import CustomLink from './Link.js';

const MDXComponents = {
  Image,
  a: CustomLink,
};

export default MDXComponents;
