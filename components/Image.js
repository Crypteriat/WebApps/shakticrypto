import Img from 'react-optimized-image';
import PropTypes from 'prop-types';

const Image = ({src, alt, ...props}) => <Img src={src} alt={alt} {...props} />;

Image.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
};

export default Image;
